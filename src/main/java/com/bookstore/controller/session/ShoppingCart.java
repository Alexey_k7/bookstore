package com.bookstore.controller.session;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Component
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ShoppingCart implements Serializable {
    private List<Integer> books = new ArrayList<>();

    public void addBook(int id) {
        books.add(id);
    }

    public void removeBook(Integer id){
        books.remove(id);
    }

    public List<Integer> getBooks() {
        return books;
    }
}
