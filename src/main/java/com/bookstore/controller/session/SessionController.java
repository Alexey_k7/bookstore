package com.bookstore.controller.session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

@RestController
public class SessionController {

    @Autowired
    private ShoppingCart shoppingCart;

    @RequestMapping("/currentUser")
    public Principal getCurrentUser(HttpServletRequest request) {
        return request.getUserPrincipal();
    }

    @RequestMapping("/addToCart")
    public void addToCart(@RequestParam("book")int bookId) {
        shoppingCart.addBook(bookId);
    }

    @RequestMapping("/removeFromCart")
    public void removeFromCart(@RequestParam("book") int bookId) {
        shoppingCart.removeBook(bookId);
    }

    @RequestMapping("/cart")
    public List<Integer> cart() {
        return shoppingCart.getBooks();
    }

}
