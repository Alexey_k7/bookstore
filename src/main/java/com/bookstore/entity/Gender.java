package com.bookstore.entity;

public enum Gender {
    MALE, FEMALE
}
