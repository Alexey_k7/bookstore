package com.bookstore.entity;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class ClientOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String bookIds;

    private String userName;

//    @Temporal(TemporalType.DATE)
//    private Date date;

//    @ManyToMany(cascade = {CascadeType.ALL})
//    @JoinTable(
//            name = "Order_Book",
//            joinColumns = {@JoinColumn(name = "order_id")},
//            inverseJoinColumns = {@JoinColumn(name = "book_id")}
//    )
//    private Set<Book> books = new HashSet<>();



//    @ManyToOne
//    private User user;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

//    public Set<Book> getBooks() {
//        return Collections.unmodifiableSet(books);
//    }
//
//    public void setBooks(Set<Book> books) {
//        this.books = books;
//    }

//    public Date getDate() {
//        return new Date(date.getTime());
//    }
//
//    public void setDate(Date date) {
//        this.date = date;
//    }

//    public User getUser() {
//        return user;
//    }
//
//    public void setUser(User user) {
//        this.user = user;
//    }


    public String getBookIds() {
        return bookIds;
    }

    public void setBookIds(String bookIds) {
        this.bookIds = bookIds;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
