package com.bookstore.entity;

public enum Role {
    ADMIN, USER
}
