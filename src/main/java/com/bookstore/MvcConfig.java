package com.bookstore;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("forward:/index.html");
        registry.addViewController("/admin").setViewName("forward:/index.html");
        registry.addViewController("/user").setViewName("forward:/index.html");
        registry.addViewController("/user/orders").setViewName("forward:/index.html");
        registry.addViewController("/user/order").setViewName("forward:/index.html");
        registry.addViewController("/login.html").setViewName("forward:/index.html");
        registry.addViewController("/addToCart").setViewName("forward:/index.html");
        registry.addViewController("/removeToCart").setViewName("forward:/index.html");
        registry.addViewController("/cart").setViewName("forward:/index.html");
        registry.addViewController("/addBook").setViewName("forward:/index.html");
    }
}
