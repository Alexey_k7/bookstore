package com.bookstore.repository;

import com.bookstore.entity.ClientOrder;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import javax.persistence.criteria.Order;

@RepositoryRestResource(collectionResourceRel = "orders", path = "orders")
public interface OrderRepository extends PagingAndSortingRepository<ClientOrder, Long> {
    ClientOrder findByUserName(String userName);
}
