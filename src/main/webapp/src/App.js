import React from 'react';
import {BrowserRouter, Route} from 'react-router-dom';

import Auth from "./components/Auth/Auth";
import AdminPanel from './components/AdminPanel/AdminPanel';

import './App.css';
import UserPanel from "./components/UserPanel/UserPanel";
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
    return (
        <BrowserRouter>
            <div className="App">
                <Route path="/login.html" component={Auth}/>
                <Route path="/admin" component={AdminPanel}/>
                <Route path="/user" component={UserPanel}/>
            </div>
        </BrowserRouter>
    );
}

export default App;
