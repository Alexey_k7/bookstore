import React from 'react';
import {Route} from 'react-router-dom'

import UserLinks from "../UserLinks/UserLinks";
import Showcase from "../Showcase/Showcase"
import TopBar from "../TopBar/TopBar";
import Order from "../Order/Order";
import UserProfile from "../UserProfile/UserProfile";
import UserOrders from "../UserOrders/UserOrders";


function UserPanel() {
    return (<div>
            <TopBar/>
            <div className="container">
                <div className="row">
                    <div className="col-3">
                        <UserLinks/>
                    </div>
                    <div className="col-9">
                        <Route path="/user/" exact component={Showcase}/>
                        <Route path="/user/order" exact component={Order}/>
                        <Route path="/user/profile" exact component={UserProfile}/>
                        <Route path="/user/orders" exact component={UserOrders}/>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default UserPanel;