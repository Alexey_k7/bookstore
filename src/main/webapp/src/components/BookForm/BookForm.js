import {Form, Input,  Button, Upload, Icon} from 'antd';
import * as React from "react";


class BookForm extends React.Component {
    state = {
        confirmDirty: false,
        autoCompleteResult: [],
    };

    handleSubmit = e => {
        console.log("sub1");
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
                this.props.onSubmit();
            }
        });
    };

    handleConfirmBlur = e => {
        const {value} = e.target;
        this.setState({confirmDirty: this.state.confirmDirty || !!value});
    };

    normFile = e => {
        console.log('Upload event:', e);
        if (Array.isArray(e)) {
            return e;
        }
        return e && e.fileList;
    };

    dummyRequest({file, onSuccess}){
        setTimeout(() =>{
            onSuccess("Ok")
        }, 0);
    }

    render() {
        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 16},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 8,
                },
            },
        };

        return (
            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                <Form.Item label="Name">
                    {getFieldDecorator('name', {
                        rules: [{required: true, message: 'Please input your nickname!', whitespace: true}],
                    })(<Input/>)}
                </Form.Item>
                <Form.Item label="Price">
                    {getFieldDecorator('price', {
                        rules: [
                            {required: true, message: 'Please input book price!',},
                        ],
                    })(<Input/>)}
                </Form.Item>
                <Form.Item label="Photo">
                    {getFieldDecorator('picture', {
                        valuePropName: 'fileList',
                        getValueFromEvent: this.normFile,
                    })(
                        <Upload name="picture" customRequest={this.dummyRequest} listType="picture">
                            <Button>
                                <Icon type="upload" /> Click to upload
                            </Button>
                        </Upload>,
                    )}
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">
                        Save
                    </Button>
                </Form.Item>
            </Form>
        );
    }
}

export default Form.create(
    {
        name: 'book_form',
        onFieldsChange(props, changedFields) {
            props.onChange(changedFields);
        },
    })(BookForm);