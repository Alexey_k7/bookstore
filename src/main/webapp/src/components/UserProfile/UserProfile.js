import React from 'react';
import axios from 'axios';
import "antd/dist/antd.css";
import UserProfileForm from "../UserProfileForm/UserProfileForm";
import moment from 'moment'
import {message} from 'antd';

class UserProfile extends React.Component {
    state = {};

    handleFormChange = changedFields => {
        this.setState(({fields}) => ({
            fields: {...fields, ...changedFields},
        }));
    };

    submit = () => {
        const msgKey = 'savingMsg';
        message.loading({content: 'Saving...', key: msgKey});
        const fields = this.state.fields;
        axios.patch(this.state.url, {
            dateOfBirth: fields.birthday.value,
            email: fields.email.value,
            gender: fields.gender.value,
            password: fields.password.value,
            username: fields.username.value
        }).then(response => {
            if (response.status === 200) {
                message.success({content: 'Saved!', key: msgKey, duration: 2});
            } else {
                message.error({content: 'Something went wrong...', key: msgKey, duration: 2});
                console.error(response);
            }
        });
    };

    componentDidMount() {
        axios.get('/currentUser').then(response => {
            axios.get('/users/search/findByUsername?username=' + response.data.name).then(response => {
                console.log(response.data);
                this.setState({url: response.data._links.self.href});
                this.setState({
                    fields: {
                        username: {
                            value: response.data.username
                        },
                        password: {
                            value: response.data.password
                        },
                        confirm: {
                            value: response.data.password
                        },
                        email: {
                            value: response.data.email
                        },
                        birthday: {
                            value: moment(response.data.dateOfBirth)
                        },
                        gender: {
                            value: response.data.gender
                        }
                    }
                });
            })
        });
    };


    render() {
        return (
            <div>
                {this.state.fields &&
                <UserProfileForm {...this.state.fields} onChange={this.handleFormChange} onSubmit={this.submit}/>}
            </div>
        );
    }
}

export default UserProfile;