import React from 'react';
import ReactTable from 'react-table';
import axios from 'axios';
import {message, Button} from 'antd';

import 'react-table/react-table.css'


class UserOrders extends React.Component {

    state = {};

    componentDidMount() {
        axios.get('/orders').then(response => {
            response.data._embedded.orders.forEach((order) => {
                order.bookIds.split(',').forEach(bookId => {
                    axios.get('findBook?id=' + bookId).then(response => {
                        this.setState(prevState => ({
                            orders: [...prevState.orders, {
                                orderId: order.id,
                                bookId: bookId,
                                name: response.data.name,
                                price: response.data.price
                            }]
                        }))
                    })
                });
            })
        });
    };

    static deleteHandler(bookId) {
        const msgKey = 'removingBookMsg';
        message.loading({content: 'Removing...', key: msgKey});
        axios.delete('/removeFromCart?book=' + bookId).then(response => {
            if (response.status === 204 || response.status === 200) {
                this.setState(function (prevState) {
                    const index = prevState.books.findIndex(book => {
                        return book.id === bookId
                    });
                    if (index > -1) {
                        prevState.books.splice(index, 1);
                        return {books: prevState.books};
                    }
                });
                message.success({content: 'Removed!', key: msgKey, duration: 2});
            } else {
                message.error({content: 'Something went wrong...', key: msgKey, duration: 2});
                console.error(response);
            }
        });
    };

    sendOrder = () => {
        const msgKey = 'sendingMsg';
        message.loading({content: 'Sanding...', key: msgKey});
        axios.get('/currentUser').then(response => {
            axios.post('/orders', {
                userName: response.data.name,
                bookIds: this.state.books.map(book => book.id).join(","),
            }).then(response => {
                if (response.status === 201) {
                    message.success({content: 'Sent!', key: msgKey, duration: 2});
                } else {
                    message.error({content: 'Something went wrong...', key: msgKey, duration: 2});
                    console.error(response);
                }
            });
        });
    };

    render() {
        console.log(this.state);
        const columns = [
            {Header: 'Book', accessor: 'name'},
            {Header: 'Price', accessor: 'price'},
            {
                Header: 'Actions',
                accessor: 'id',
                Cell: ({value}) => <button onClick={UserOrders.deleteHandler.bind(this, value)}>Delete</button>
            }
        ];

        return (
            <div>
                <h1>Orders</h1>
                {this.state.orders && this.state.orders[0] && this.state.orders[0].bookId}
            </div>
        );
    }
}

export default UserOrders;