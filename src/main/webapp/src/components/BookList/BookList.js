import React from 'react';
import ReactTable from 'react-table';
import {Modal, Button, message} from 'antd';
import axios from 'axios';

import 'react-table/react-table.css'
import BookForm from "../BookForm/BookForm";


class BookList extends React.Component {

    state = {showAddBookForm: false};

    componentDidMount() {
        axios.get('/books').then(response => {
            console.log(response.data._embedded.books);
            this.setState({books: response.data._embedded.books})
        });
    };

    deleteHandler(resource) {
        axios.delete(resource._links.self.href).then(response => {
            if (response.status === 204 || response.status === 200) {
                this.setState(function (prevState) {
                    const index = prevState.books.indexOf(resource);
                    if (index > -1) {
                        prevState.books.splice(index, 1);
                        return {books: prevState.books};
                    }
                });
            }
        });
    };

    submit = () => {
        console.log('Submitted');
        console.log(this.state.fields);
        const formData = new FormData();
        console.log('test11 ' + this.state.fields.picture);
        console.log('test22 ' + this.state.fields.picture.value[0]);
        formData.append("file", "test");
        axios.post('/addBook', formData).then(response => {alert(response.data)});
        // fetch('/addBook', {
        //     method: 'post',
        //     body: formData
        // }).then(res => {
        //     if(res.ok) {
        //         console.log(res.data);
        //         alert("File uploaded successfully.")
        //     }
        };


        // const msgKey = 'savingMsg';
        // message.loading({content: 'Saving...', key: msgKey});
        // const fields = this.state.fields;
        // axios.patch(this.state.url, {
        //     dateOfBirth: fields.birthday.value,
        //     email: fields.email.value,
        //     gender: fields.gender.value,
        //     password: fields.password.value,
        //     username: fields.username.value
        // }).then(response => {
        //     if (response.status === 200) {
        //         message.success({content: 'Saved!', key: msgKey, duration: 2});
        //     } else {
        //         message.error({content: 'Something went wrong...', key: msgKey, duration: 2});
        //         console.error(response);
        //     }
        // });


    handleFormChange = changedFields => {
        this.setState(({fields}) => ({
            fields: {...fields, ...changedFields},
        }));
    };

    addBook = () => {
        this.setState({showAddBookForm: true});
    };

    closeAddBookForm = () => {
        this.setState({showAddBookForm: false});
    };

    render() {
        console.log(this.state);

        const columns = [
            {Header: 'Name', accessor: 'name'},
            {Header: 'Price', accessor: 'price'},
            {Header: 'Picture', accessor: 'picture'},
            {
                Header: 'Actions',
                Cell: props => <button onClick={this.deleteHandler.bind(this, props.original)}>Delete</button>
            }];

        return (
            <div>
                <h1>Books</h1>
                <Button type="primary" onClick={this.addBook}>Add</Button>
                <Modal title="Add book" visible={this.state.showAddBookForm} onOk={this.closeAddBookForm}
                       onCancel={this.closeAddBookForm}>
                    <BookForm onSubmit={this.submit} onChange={this.handleFormChange}/>
                </Modal>
                <ReactTable data={this.state.books} columns={columns} minRows={3}/>
            </div>
        );
    }
}

export default BookList;