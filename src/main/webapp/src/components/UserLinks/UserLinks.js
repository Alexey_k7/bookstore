import React from 'react';
import {Link} from 'react-router-dom';

function UserLinks() {
    return (
        <div>
            <ul className="nav flex-column nav-pills">
                <li><Link to="/user/profile" class="nav-link">Profile</Link></li>
                <li><Link to="/user/orders" class="nav-link">Orders</Link></li>
            </ul>
        </div>
    );
}

export default UserLinks;