import React from 'react';
import axios from 'axios';

class TopBar extends React.Component {

    state = {};

    componentDidMount() {
        axios.get('/currentUser').then(response => {
            this.setState({
                currentUser: response.data.name,
                isAdmin: response.data.authorities[0].authority === 'ROLE_ADMIN'
            });
        });
    };

    render() {
        return (<div>
                <div style={{background: '#FFF8DC'}}>
                    <div className="container">
                        <span style={{float: 'left'}}>Hello <b>{this.state.currentUser}</b></span>
                        <span style={{float: 'right'}}><a href="/logout">Logout</a> </span>
                        {!this.state.isAdmin && <span><a href="/user/order">Current Order</a></span>}
                    </div>
                </div>
                <br/>
            </div>
        );
    }
}

export default TopBar;