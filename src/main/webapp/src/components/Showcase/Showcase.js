import React from 'react';
import axios from 'axios';
import {Pagination, Card, Col, Row} from "antd";
import "antd/dist/antd.css";
import {message} from 'antd';


class Showcase extends React.Component {

    state = {};

    componentDidMount() {
        axios.get('/books').then(response => {
            console.log(response.data._embedded.books);
            this.setState({books: response.data._embedded.books});
            this.setState(prevState => ({booksPage: prevState.books.slice(0,9)}))
        });
    };

    pageChanged = (page, pageSize) => {
        this.setState(prevState => ({booksPage: prevState.books.slice((page - 1) * pageSize, page * pageSize)}));
    };


    static addBookHandler(bookId) {
        const msgKey = 'addingBookMsg';
        message.loading({content: 'Saving...', key: msgKey});
        axios.post('/addToCart?book=' + bookId).then(response => {
            if (response.status === 200) {
                message.success({content: 'Added!', key: msgKey, duration: 2});
            } else {
                message.error({content: 'Something went wrong...', key: msgKey, duration: 2});
                console.error(response);
            }
        });
    };

    render() {
        console.log(this.state.booksPage);
        return (
            <div>
                <h1>Books</h1>
                <div style={{background: '#ECECEC', padding: '20px'}}>
                    <Row gutter={16}>
                        {this.state.booksPage && this.state.booksPage.slice(0, 3) && this.state.booksPage.slice(0, 3).map(book =>
                            <Col span={8}>
                                <Card title={book.name} bordered={false}>
                                    <img src={process.env.PUBLIC_URL + '/no-cover.gif'}/><br/>
                                    <b>Price: </b> {book.price} <br/>
                                    <button onClick={Showcase.addBookHandler.bind(this, book.id)}>Add</button>
                                </Card>
                            </Col>)}
                    </Row>
                </div>
                <div style={{background: '#ECECEC', padding: '20px'}}>
                    <Row gutter={16}>
                        {this.state.booksPage && this.state.booksPage.slice(3, 6) && this.state.booksPage.slice(3, 6).map(book =>
                            <Col span={8}>
                                <Card title={book.name} bordered={false}>
                                    <img src={process.env.PUBLIC_URL + '/no-cover.gif'}/><br/>
                                    <b>Price: </b> {book.price} <br/>
                                    <button onClick={Showcase.addBookHandler.bind(this, book.id)}>Add</button>
                                </Card>
                            </Col>)}
                    </Row>
                </div>
                <div style={{background: '#ECECEC', padding: '20px'}}>
                    <Row gutter={16}>
                        {this.state.booksPage && this.state.booksPage.slice(6, 9) && this.state.booksPage.slice(6, 9).map(book =>
                            <Col span={8}>
                                <Card title={book.name} bordered={false}>
                                    <img src={process.env.PUBLIC_URL + '/no-cover.gif'}/><br/>
                                    <b>Price: </b> {book.price} <br/>
                                    <button onClick={Showcase.addBookHandler.bind(this, book.id)}>Add</button>
                                </Card>
                            </Col>)}
                    </Row>
                </div>
                <Pagination defaultCurrent={1} pageSize={9} total={this.state.books && this.state.books.length}
                            onChange={this.pageChanged}/>
            </div>
        );
    }
}

export default Showcase;