import {Form, Input, Select, Button, DatePicker} from 'antd';
import * as React from "react";

const {Option} = Select;

class UserProfileForm extends React.Component {
    state = {
        confirmDirty: false,
        autoCompleteResult: [],
    };

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
                this.props.onSubmit();
            }
        });
    };

    handleConfirmBlur = e => {
        const {value} = e.target;
        this.setState({confirmDirty: this.state.confirmDirty || !!value});
    };

    compareToFirstPassword = (rule, value, callback) => {
        const {form} = this.props;
        if (value && value !== form.getFieldValue('password')) {
            callback('Two passwords that you enter is inconsistent!');
        } else {
            callback();
        }
    };

    validateToNextPassword = (rule, value, callback) => {
        const {form} = this.props;
        if (value && this.state.confirmDirty) {
            form.validateFields(['confirm'], {force: true});
        }
        callback();
    };

    render() {
        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 16},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 8,
                },
            },
        };

        return (
            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                <Form.Item label="Username">
                    {getFieldDecorator('username', {
                        rules: [{required: true, message: 'Please input your nickname!', whitespace: true}],
                    })(<Input/>)}
                </Form.Item>
                <Form.Item label="Password" hasFeedback>
                    {getFieldDecorator('password', {
                        rules: [
                            {required: true, message: 'Please input your password!',},
                            {validator: this.validateToNextPassword,},
                        ],
                    })(<Input.Password/>)}
                </Form.Item>
                <Form.Item label="Confirm Password" hasFeedback>
                    {getFieldDecorator('confirm', {
                        rules: [
                            {required: true, message: 'Please confirm your password!',},
                            {validator: this.compareToFirstPassword,},
                        ],
                    })(<Input.Password onBlur={this.handleConfirmBlur}/>)}
                </Form.Item>
                <Form.Item label="E-mail">
                    {getFieldDecorator('email', {
                        rules: [
                            {type: 'email', message: 'The input is not valid E-mail!',},
                            {required: true, message: 'Please input your E-mail!',},
                        ],
                    })(<Input/>)}
                </Form.Item>
                <Form.Item label="Birthday">
                    {getFieldDecorator('birthday', {
                        rules: [{required: true, message: 'Please input your Birthday!'}]
                    })(<DatePicker/>)}
                </Form.Item>
                <Form.Item label="Gender">
                    {getFieldDecorator('gender', {
                        rules: [{required: true, message: 'Please select your gender!'}],
                    })(
                        <Select
                            placeholder="Select a option and change input text above"
                            onChange={this.handleSelectChange}>
                            <Option value="MALE">male</Option>
                            <Option value="FEMALE">female</Option>
                        </Select>,
                    )}
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">
                        Save
                    </Button>
                </Form.Item>
            </Form>
        );
    }
}

export default Form.create(
    {
        name: 'user_form',
        mapPropsToFields(props) {
            return {
                username: Form.createFormField({
                    ...props.username,
                    value: props.username.value,
                }),
                password: Form.createFormField({
                    ...props.password,
                    value: props.password.value,
                }),
                confirm: Form.createFormField({
                    ...props.confirm,
                    value: props.confirm.value,
                }),
                email: Form.createFormField({
                    ...props.email,
                    value: props.email.value,
                }),
                birthday: Form.createFormField({
                    ...props.birthday,
                    value: props.birthday.value,
                }),
                gender: Form.createFormField({
                    ...props.gender,
                    value: props.gender.value,
                }),
            };
        },
        onFieldsChange(props, changedFields) {
            props.onChange(changedFields);
        },
    })(UserProfileForm);