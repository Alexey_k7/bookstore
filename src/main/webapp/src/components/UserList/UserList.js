import React from 'react';
import ReactTable from 'react-table';
import axios from 'axios';

import 'react-table/react-table.css'


class UserList extends React.Component {

    state = {};

    componentDidMount() {
        axios.get('/users').then(response => {
            this.setState({users: response.data._embedded.users})
        });
    };

    static deleteHandler(resource) {
        axios.delete(resource._links.self.href).then(response => {
            if (response.status === 204) {
                this.setState(function (prevState) {
                    const index = prevState.users.indexOf(resource);
                    if (index > -1) {
                        prevState.users.splice(index, 1);
                        return {users: prevState.users};
                    }
                });
            }
        });
    };

    render() {

        const columns = [
            {Header: 'Username', accessor: 'username'},
            {Header: 'Gender', accessor: 'gender'},
            {Header: 'Email', accessor: 'email'},
            {
                Header: 'Actions',
                Cell: props => <button onClick={UserList.deleteHandler.bind(this, props.original)}>Delete</button>
            }];

        return (
            <div>
                <h1>Users</h1>
                <ReactTable data={this.state.users} columns={columns}/>
            </div>
        );
    }
}

export default UserList;