import React from 'react';
import {Route} from 'react-router-dom'

import UserList from '../UserList/UserList.js';
import BookList from "../BookList/BookList";
import AdminLinks from "../AdminLinks/AdminLinks";
import TopBar from "../TopBar/TopBar";
import UserProfile from "../UserProfile/UserProfile";

function AdminPanel() {
    return (<div>
            <TopBar/>
            <div className="container">
                <div className="row">
                    <div className="col-3">
                        <AdminLinks/>
                    </div>
                    <div className="col-9">
                        <Route path="/admin" exact component={UserProfile}/>
                        <Route path="/admin/users" exact component={UserList}/>
                        <Route path="/admin/books" exact component={BookList}/>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default AdminPanel;