import React from 'react';
import {Link} from 'react-router-dom';

function AdminLinks() {
    return (
        <div>
            <ul className="nav flex-column nav-pills">
                {/*TODO: Fix nested routes*/}
                <li><Link to="/admin" class="nav-link">Profile</Link></li>
                <li><Link to="/admin/users" class="nav-link">Users</Link></li>
                <li><Link to="/admin/books" class="nav-link">Books</Link></li>
            </ul>
        </div>
    );
}

export default AdminLinks;